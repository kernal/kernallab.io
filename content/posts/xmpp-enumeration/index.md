---
title: "XMPP enumeration and hardening"
date: 2023-09-03T17:51:00+00:00
author: "Siren"
---

[![banner](digilol-pentest-banner.png)](https://digilol.net)

XMPP is an open standard mostly used for messaging. There are many server/client implementations and protocol extensions for XMPP. Servers can also be configured to federate with each other.

An XMPP server belonging to an organization can leak crucial information such as employee names and titles, IP addresses and client information. An XMPP server can also be prone to MITM when encryption isn't enforced during federation or client-to-server events.

## SRV records
In order for users to have addresses (JIDs) such as `username@kernal.eu` when the XMPP server is really deployed on `xmpp.kernal.eu`, SRV records are used. These records work the same way as email MX records.

These record names are standard so they can be used for discovering XMPP servers.

- `_xmpp-client._tcp.example.net.`
- `_xmpps-client._tcp.example.net.`
- `_xmpp-server._tcp.example.net.`
- `_xmpps-server._tcp.example.net.`

See examples [here](https://wiki.xmpp.org/web/SRV_Records).

## Enumerating supported server features
Using [XEP-0030: Service Discovery](https://xmpp.org/extensions/xep-0030.html) you can enumerate available features and entities such as [multi-user chats (MUCs)](https://xmpp.org/extensions/xep-0045.html) and [pubsub](https://xmpp.org/extensions/xep-0060.html) services. You can use [Pidgin](https://pidgin.im/)'s XMPP console to send requests in XML.

Request:
```xml
<iq from='siren@kernal.eu/H60u9GDMmqQh'
    id='ux71f325'
    to='kernal.eu'
    type='get'>
  <query xmlns='http://jabber.org/protocol/disco#info'/>
</iq>
```

Response:
```xml
<iq to='siren@kernal.eu' id='ux71f325' from='kernal.eu' type='result'>
	<query xmlns='http://jabber.org/protocol/disco#info'>
		<identity type='im' name='Prosody' category='server'/>
 		<item jid='chat.kernal.eu' name='Chatroom Service'/>
		<feature var='jabber:iq:private'/>
		<feature var='msgoffline'/>
		<feature var='urn:xmpp:blocking'/>
		<feature var='vcard-temp'/>
		<feature var='jabber:iq:roster'/>
		<feature var='http://jabber.org/protocol/disco#info'/>
		<feature var='http://jabber.org/protocol/disco#items'/>
		<feature var='urn:xmpp:carbons:rules:0'/>
		<feature var='urn:xmpp:carbons:2'/>
	</query>
</iq>
```

Then you can proceed to check the supported features and entities.

### Some interesting features
- [XEP-0054: vcard-temp](https://xmpp.org/extensions/xep-0054.html): Retrieve vcards for users.
- [XEP-0084: User Avatar](https://xmpp.org/extensions/xep-0084.html): Avatars can be retrieved from attacker controlled HTTP endpoints.
- [XEP-0279: Server IP Check](https://xmpp.org/extensions/xep-0279.html)
- [XEP-0080: User Location](https://xmpp.org/extensions/xep-0080.html)
- [XEP-0321: Remote Roster Management](https://xmpp.org/extensions/xep-0321.html): Server's default buddy list.
- [XEP-0055: Jabber Search](https://xmpp.org/extensions/xep-0055.html): Supports wildcard search. Can be used to leak complete list of users.

### Enumerating Multi-User Chat (MUC) supported features
Request:
```xml
<iq from='siren@kernal.eu/YgrsUq2CfU_b' id='lx09df27' to='chat.kernal.eu' type='get'>
  	<query xmlns='http://jabber.org/protocol/disco#info'/>
</iq>
```

Response:
```xml
<iq to='siren@kernal.eu/YgrsUq2CfU_b' id='lx09df27' from='chat.kernal.eu' type='result'>
	<query xmlns='http://jabber.org/protocol/disco#info'>
		<identity type='text' name='Prosody Chatrooms' category='conference'/>
		<feature var='urn:xmpp:occupant-id:0'/>
		<feature var='http://jabber.org/protocol/muc'/>
		<feature var='http://jabber.org/protocol/commands'/>
		<feature var='http://jabber.org/protocol/disco#info'/>
		<feature var='http://jabber.org/protocol/disco#items'/>
		<feature var='http://jabber.org/protocol/muc#unique'/>
	</query>
</iq>
```

## Hardening
- Disallow cleartext S2S and C2S.
- Check if anonymous logins and public registrations are allowed. Disable if you don't intend to have them.

## Links
- https://wiki.xmpp.org/web/Securing_XMPP
- https://bishopfox.com/blog/xmpp-underappreciated-attack-surface
- https://prosody.im/doc/security
