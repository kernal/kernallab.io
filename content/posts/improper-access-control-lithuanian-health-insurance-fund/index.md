---
title: "Improper Access Control (National Health Insurance Fund in Lithuania)"
date: 2021-03-13T04:00:00+00:00
author: "Stnby"
---

## Background information
The following vulnerability was reported to the National Cyber Security Centre ("NKSC") and patched within days.

**Access control** is a security process that controls usage of specific resources within a predefined criteria.

**"National Health Insurance Fund under the Ministry of Health"** also known as "VLK" in Lithuania
is an institution that manages a lot of health related data **well obviously**.

They have a web-application through which you can view your medical records and do some basic tasks, none of which are in the scope of this post.

{{< figure src="2021-03-13_03-27.png" title="Front-end or National Health Insurance Fund web-application" >}}

## Deploying without testing
You might assume that a governmental organization that manages a lot personal data (for every single citizen in the Republic of Lithuania)
would actually test the web-application before deploying it to production?\
Well I thought the same, until I saw it myself *(and it did not even require a lot of digging)*.

## Discovery of the vulnerability
### 1. Finding interesting endpoints
By browsing throughout the UI we can observe all of the endpoints.
{{< figure src="2021-03-13_03-38.png" title="One of the interesting endpoints" >}}

There were 2 interesting endpoints provided by [SAP HANA](https://www.sap.com/products/hana.html) which is a in-memory database.

 1. `https://e.vlk.lt/xsodata/veptp.xsodata/`
 2. `https://e.vlk.lt/xsodata/esdk.xsodata/`

### 2. Doing some research
We can find a [SAP HANA Search Developer Guide](https://help.sap.com/doc/95545fb91e744a87a43fae1ca08a61cf/2.0.05/en-US/SAP_HANA_Search_Developer_Guide_en.pdf) on the Internet.

{{< figure src="Screenshot_2021-03-13 SAP HANA Search Developer Guide - SAP_HANA_Search_Developer_Guide_en pdf.png" title="Method 'GET' - $metadata Call" >}}

The following method returns the meta-data for all views that the **user is allowed to access**.

### 3. Trying out luck
If we try out the `$metadata` call we can find some interesting information.
{{< figure src="2021-03-01_14-32.png" title="Testing $metadata call" >}}

By following the specification and meta-data we can find how to use this API and try to get some user data.
{{< figure src="2021-03-13_04-13.png" title="Finding persons ID and a full name" >}}
{{< figure src="2021-03-13_04-11.png" title="Finding persons living address" >}}

There is no point in looking deeper it is obvious that we have an issue right here.

### 4. Responsibly disclosing the vulnerability to the authorities and waiting for response
 * **2021-03-01** - Sent out a GPG encrypted email with the details to the authorities;
 * **2021-03-01** - Received some feedback;
 * **2021-03-02** - Web-application was taken down;
{{< figure src="2021-03-02_18-37.png" title="Web-application taken down" >}}
 * **2021-03-03** - Vulnerability was fixed and web-application restored;
