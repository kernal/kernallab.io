---
title: "Migrating a Matrix server (Synapse)"
date: 2021-09-14T16:40:48+03:00
draft: false
author: "Siren"
---

We apologize for the inconveniences caused by our 5 day Matrix downtime. Some of our sysadmins got stranded at weird corners of the world.
Although there is a lack of documentation on migration, migrating Synapse is fairly easy.

Here is a checklist of what you need to do:
* Make sure Synapse isn't running.
* Backup the Synapse database.
* Copy over the media store, homeserver configuration files and keys.
* If needed update your DNS records and configure a reverse proxy.

Same goes if you are running Synapse within Docker since it uses volumes.

## Here's a script that does it:
```sh
#!/bin/sh
# This script is meant to be run on the new homeserver.
# It will copy the keys, configuration files, media store and the database.

OLD_SERVER_SSH=nowotwor.kernal.eu
OLD_SERVER_DB=synapse # Old homeserver database name
SERVER_NAME=kernal.eu # New homeserver name

BASE=synapse_$SERVER_NAME
DATA=$BASE/data # If docker it's /data, otherwise under /var/lib

mkdir -p $DATA

# Copy over media store, homeserver conf and keys
rsync -avz root@$OLD_SERVER_SSH:/var/lib/synapse/media_store $DATA
rsync -avz root@$OLD_SERVER_SSH:/etc/synapse/ $DATA

# Prepare database user
su -lc "createuser --pwprompt $BASE" postgres
# Prepare database
su -lc "createdb --encoding=UTF8 --locale=C --template=template0 --owner=$BASE $BASE" postgres

# Backup and restore database
ssh root@$OLD_SERVER_SSH "su -lc 'pg_dump $OLD_SERVER_DB > /tmp/$OLD_SERVER_DB' postgres"

rsync -avz root@$OLD_SERVER_SSH:/tmp/$OLD_SERVER_DB /tmp/$OLD_SERVER_DB

chown postgres:postgres /tmp/$OLD_SERVER_DB

su -c "psql -U $BASE -d $BASE -f /tmp/$OLD_SERVER_DB" postgres

# Cleanup
rm -f /tmp/$OLD_SERVER_DB
```

After deploying if your homeserver has trouble communicating with other homeservers, make sure you have all of your old keys and secrets in homeserver.yaml.
