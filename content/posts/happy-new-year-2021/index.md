---
title: "Happy New Year 2021"
date: 2020-12-31T18:37:51+03:00
author: "Kernal"
---
This year has been a race condition. A race condition of a bunch of shit with fuck-all memory management. Perhaps that's why the year crashed this fast.

Some good things also happened such as:
 * We got some new members. (primarily from our onion service)
 * Introduction of a new server called [doner.kernal.eu](https://doner.kernal.eu) which hosts our mail server.
 * We ended the year with quite a few interesting data breaches. (Upcoming posts soon)
 * We made this dank ass painting featuring our most *fabulous* (ex)team members.

{{< figure src="Painting.png" title="kCribs - Drawing by Siren, GIMP'd by Stnby" >}}

From left to right:
 * **Dong**, the Vietnamese Kali Lincox 1337 hacker
 * **WhatsInMyVape**, some Irish dude who can't speak his mother tongue. Has hemorrhoids and likes to vape.
 * **Etienne**, UK-French baguette-toast. Dude who likes to flex about having (gyno) intercourses with women from the hotel he works at. (Hence the latex gloves)
 * **Stnby**, TV remote, part of the CRT TV gang. Lithuanian aurora borealis.
 * **Duxz/Crtoff**, Dutchman has the highest WPM and MPM (messages per minute). Also known as mr. nice soft boy hands.
 * **Siren**, 1337 elite girl (didn't know girls can hax this well). The C master chef. *(Has severe autizm)*

The paintings:
 * **Munstie**, the victim of Apple's marketing virus. Source of all our diversity points, from the UK.
 * **ZoltanZuberi**, wholesome Pakistani guy whose aim was to hack his ex-gf. Has a big smile.
 * **WhatsInMyVape**, our personal camboy's nipples.

Jokes aside, we wish everyone reading this a happy new year with a clean new stack.
```sh
sed 's/corona/1337/g'
```

Love from Antarctica,\
Kernal Kernovich
