---
title: "Turkish Citizenship Database v2 (Kids Edition)"
date: 2021-01-07T09:38:43+03:00
author: Siren
---

## Some Background
* Raunt (RNPlayer) and VCloud (VCollabPlayer) are both remote education software made by a Turkish software company called Sebit (who also has a past of making several popular education platforms, it's a shame to this day they still haven't learned or simply don't care about information security).

* There are only two instances of these software: Sebit's own instance and Turkey's Ministry of Education network (yes, a government instance).

I have attempted to inform Sebit by sending multiple emails telling them that I have found a vulnerability and asking for their GPG key. It's been a month they haven't responded. But apparently they aren't using the API endpoints on Raunt anymore so I think it's appropriate to make a post on this. I bet their new approach also has some holes but that's gonna be another post.

## The Data Exposure

### RNPlayer < v1.0.372

Users log in using their passport number and password. By default the passwords are the first few digits of their passport number. Using passport number for authentication is stupid to begin with.

Raunt relies on the client to filter the response returned by the API. Directly accessing the API is easy and leaks a ton of data.

{{< figure src="Raunt.png" title="Viewing members of a group" >}}

Usernames are Turkish Identification/passport numbers. The `pwdReset` shows whether an user has changed the default password or not. Which makes it possible to log in as users who haven't done a password change as we know it's going to be the first digits of their username.

Crawling through assignments to find other groups is also possible.\
The same group and user IDs are also available on their other platform VCloud.

### VCollabPlayer

There are three types of organizations/groups: class, school, corporation. An user is allowed to view profiles of only the class group members. But that's just the UI of course. API spits out everything.

{{< figure src="Vcloudcorp.png" title="Members of a corporation" >}}

{{< figure src="Vclouduser.png" title="Retrieving contact information of a random user" >}}

{{< figure src="Vcloudip.png" title="It's absurd, not important" >}}

## The Scope

Turkish government was previously running Raunt but it seems that recently they switched to VCloud. All of the above goes for their instance as well. And considering there are millions of kids on their platform you might as well call it the Turkish Citizenship DB v2.

## Extra

While I was trying to figure out what process should I follow to report a vulnerability, I found out that Turkey does not have any related cyber-security laws. The existing data protection law does not dictate how and when should one go and report a vulnerability. Neither the vendors know how to respond.
