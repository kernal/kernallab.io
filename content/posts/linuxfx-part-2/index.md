---
title: "Linuxfx: Revenge of the Skids"
date: 2022-05-26T05:09:06+00:00
author: Satya Nutella
---

## Introduction
This post is an update to the [previous post on Linuxfx](../linuxfx/). Please read that first if you haven't done so.

## Aftermath
Several hours after DistroTube uploaded a [video on the previous post](https://odysee.com/@DistroTube:2/proprietary-distro-linuxfx-has-user:6), the Linuxfx "developers" revoked the leaked database credentials after people started griefing the database. However, they did not properly fix the underlying issue, leaving the database wide open for intrusion.

## More dynamic analysis
I downloaded the latest Linuxfx installation media and threw it in a libvirt VM just like last time. I noticed a new string in the GUI activation client binary:

```sh
pkexec killall -9 sudo
```

It seems like the skids actually modified the activation client to try and protect itself against dynamic analysis. (Un)fortunately this one in particular can be bypassed by using `pkexec` for elevation instead of `sudo`.

To work around any potential self protection, I copied `/usr/bin/bpftrace` to `/tmp/systemd` and ran a modified version of the command in the last post:

```sh
pkexec /tmp/systemd -e 'uprobe:/usr/lib/x86_64-linux-gnu/libmysqlclient.so.21:mysql_real_connect { printf("host=%s user=%s password=%s database=%s port=%d\n", str(arg1), str(arg2), str(arg3), str(arg4), arg5); }'
```

This gives us the following:

```
host=181.215.242.77 user=fxuser password=99D28/gQxU%WY database=newbase port=19708
```

Connecting to the database shows that the credentials are valid.

## Even more dynamic analysis
I'm still interested in finding out exactly what the program executes, what files does the program opens, etc.

Installing `bpfcc-tools` and copying `/usr/bin/execsnoop-bpfcc` to `/tmp/systemd` and running:
```sh
pkexec /tmp/systemd
```

and then opening the activation client gives us the following interesting output:

{{< figure src="fae5fde3a0259333362a1b195bf4d962a86d43614afaf19f98c87917c07d6ae0.png" title="execsnoop-bpfcc output" >}}

The activation program seems to check for the presence of `sudo`, `stap` (the [SystemTap](https://sourceware.org/systemtap/) system tracing utility), and anything containing `bp`, which will catch `bpftrace` and `*-bpfcc` tools. When it detects such programs, the following notification is shown:

{{< figure src="4deb427d772b651e08d4bc2b822f994ffebb62ced71d94220b9ef1fa106c1a82.png" title="Fake error message" >}}

The program also uses `curl` to download data from the following URLs: `http://www.linuxfx.org/linuxfx/x86/11.1/.http` and `http://www.linuxfx.org/linuxfx/x86/11.1/.https`. The first URL contains the database type, port, host, database and username. The second URL contains the database password:

```
$ curl http://www.linuxfx.org/linuxfx/x86/11.1/.http
mysql
19708
181.215.242.77
newbase
fxuser
$ curl http://www.linuxfx.org/linuxfx/x86/11.1/.https
99D28/gQxU%WY
```

This means that the activation program doesn't store any credentials in its binary, but it's stored on unauthenticated HTTP endpoints that anybody can access.

Repeating the same steps for `opensnoop-bpfcc` and running it with the `-n gbr3` argument shows that the program reads the hardware ID from `/boot/.grubkey` and writes the key to `/opt/Linuxfx/wxdesktop/.key`.

**Q.E.D.**

## Conclusion
Not only did the skids from Linuxfx Software fail to learn anything, they even tried to further rely on "security through obscurity" by attempting to prevent dynamic analysis of the system, which failed spectacularly as you have seen above.

As proof for the database compromise, the following key can be used to activate virtually infinite Linuxfx machines:
```
00-4200-9999-999999999999
```
