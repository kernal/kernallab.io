---
title: "Perfect movie watching experience (syncplay, qbittorent-nox, nginx)"
date: 2021-01-10T14:50:00+02:00
author: "Stnby"
---
Do you like watching movies with friends? **Every night???**\
Even tho it is the *middle of pandemic* but it does not matter as they live thousands of nautical miles away anyway?
Or maybe torrenting seem a bit tedious and inconvenient as everyone having to download same file?

**Read ahead our setup has none of the above mentioned flaws. :)**

Here we are going to use **qBittorent-nox** as our torrent client.\
We are going to use **Nginx** to serve downloaded files for streaming.\
We also are going to spin our own **Syncplay** server instance.

## Server end setup
Requirements:
 * [qBittorrent-nox](https://www.qbittorrent.org/) — qBittorrent just without X.
 * [Nginx](https://nginx.org) — High Performance Load Balancer, Web Server, & Reverse Proxy.
 * [Syncplay](https://syncplay.pl/) — FOSS software that synchronizes media players.

These should probably be in your repositories.

For this setup **I recommend creating a spare user for qBittorent daemon** (Nothing is perfect especially Python. Last thing we want is our personal user account getting breached or even worse root!!!).

```sh
useradd -m -s /bin/sh qbittorent
chmod 777 /home/qbittorent # We do this so nginx will be able to read our files.
```

### qBittorrent-nox service setup
**Run these on previously created user.**
```sh
qbittorrent-nox --webui-port=8081 -d
```

Add this to your crontab so it starts up on boot as well.
```cron
@reboot qbittorrent-nox --webui-port=8081 -d
```

Login into the Web UI with default credentials: `admin:adminadmin`.
{{< figure src="2021-01-10_02-28.png" title="Login for qBittorent Web UI" >}}

Change the default password. I mean seriously if you think this is safe may I remind you that anyone can install a Python plug-in and take over your system.

{{< figure src="2021-01-10_02-48.png" title="navigate to: Tools > Options... > Web UI" >}}

### Nginx configuration
Symlink our `/home/qbittorent/Downloads` directory somewhere managed by Nginx server.

```sh
ln -s /home/qbittorent/Downloads /var/www/html/Torrents
```

Here is a snippet of my Nginx configuration `/etc/nginx/nginx.conf`.\
If you don't understand anything do this part yourself.

```nginx
http {
	# Make sure symlinks are allowed.
	disable_symlinks off;

	server {
		listen 443 ssl;
		server_name doner.kernal.eu;
		# More boring SSL stuff...

		# qBittorrent Web UI.
		location /qbt/ {
			proxy_pass http://127.0.0.1:8081/;
			proxy_http_version 1.1;
			proxy_set_header X-Forwarded-Host $http_host;
		}

		# Downloaded files.
		location /Torrents/ {
			autoindex on;
			root /var/www/html/Torrents;
		}
	}
}
```

This is how everything should look now.

{{< figure src="2021-01-10_03-21.png" title="qBittorent Web UI proxied over Nginx" >}}
{{< figure src="2021-01-10_03-25.png" title="Serving downloaded files over Nginx" >}}

I know you are about to give that URL a visit. Don't even bother lol.

### Setting up Syncplay server
Just add this to the crontab of qbittorent user.\
Why over complicate things?

```cron
@reboot syncplay-server --password supersecurepass --salt AJTCUVKLKJ
```

## Client end setup
Requirements:
 * [mpv](https://mpv.io/) — A free, open source, and cross-platform media player.
 * [Syncplay](https://syncplay.pl/) — FOSS software that synchronizes media players.

I have no idea why but don't even try using the GUI for Syncplay.\
**Spoiler it just [does not work](https://github.com/Syncplay/syncplay/issues/321).** Using command line works good enough tho.

I like to set this alias.
```sh
alias syncplay="syncplay --no-gui -a syncplay.kernal.eu -n $USER -p supersecurepass -r funroom --player-path /usr/bin/mpv"
```

To play a video run:
```sh
syncplay 'https://doner.kernal.eu/Torrents/The.Matrix.1999.1080p.BrRip.x264.YIFY.mp4'
```

Same but with subtitles file:
```sh
syncplay 'https://doner.kernal.eu/Torrents/The.Matrix.1999.1080p.BrRip.x264.YIFY.mp4' -- \
	--sub-file='coolsubs.srt'
```
{{< figure src="2020-12-05_23-38.png" title="Syncplay, mpv and a weird dude without a mic" >}}

Done. Now all you would want to do is get on a Mumble server and laugh. (post upcoming)
