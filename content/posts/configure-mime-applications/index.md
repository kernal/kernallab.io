---
title: "Configure XDG MIME applications"
date: 2021-01-08T17:05:00+02:00
author: "Stnby"
---
Have you ever had your `.pdf` file being opened with GIMP by default? Or maybe even worse `.txt` file opened by Notepad via Wine?

Well good news. This post is exactly for you.

## How does it work?
GUI application usually includes a [Desktop Entry](https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html) file. Which describes what MIME types it can handle.

## Usage examples
### View all XDG desktop entries
```sh
echo $XDG_DATA_DIRS | tr -d '\n' | xargs -d : -I % find %/applications -name '*.desktop'
```
```
...
gimp.desktop
mplayer.desktop
mpv.desktop
mumble.desktop
mupdf.desktop
sxiv.desktop
vlc.desktop
...
```

### Find MIME type for a chosen file
```
$ xdg-mime query filetype ~/Downloads/Risen.pdf
application/pdf
```

### Get current defaults for specified MIME type
This command prints out a default application for `.pdf` files. Which is GIMP!!!
```
$ xdg-mime query default application/pdf
gimp.desktop
```

### Change the default application for a MIME type
This command sets MuPDF as default application for `.pdf` files.\
*Instead of previously used GIMP.*
```
xdg-mime default mupdf.desktop application/pdf
```

### Open a file with a default application
```
xdg-open ~/Downloads/Risen.pdf
```
{{< figure src="2021-01-08_22-26.png" title="Using xdg-open to open a file" >}}
