---
title: My IMEI is dead - Restoring my IMEI
date: 2022-09-03T18:00:00+0000
Author: Nima Fanniasl
---

One day, I wanted to switch my custom ROM and I downloaded the new one and rebooted to TWRP to flash it and I didn't have any backups of system partitions like EFS! I flashed the custom ROM but my phone broke.

even the splash screen of my phone was broken and nothing worked! when I rebooted my phone it got stuck in TWRP and rebooted to TWRP again and again and again, I tried flashing a custom ROM like lineage that I know was working but when I flashed it nothing happened again and it was stuck in TWRP.

I downloaded MIUI (My phone's stock rom) and I flashed it and everything worked fine but I didn't realize that my SIM card was not working.

I rebooted to recovery and installed lineage but when I installed it and I set it up. I found out that my SIM card does not work. 

I tried `*#06#` but nothing happened! I checked my IMEI from settings and IMEI1 and IMEI2 were 0! dead IMEI :(

I tried switching my ROM again and clean installing lineage, but nothing happened and then I started to try different methods to restore my IMEI.

I didn't find any good tutorial for restoring IMEI So I raised my problem in xda and some other forums and didn't get an answer.

I was able to restore my xiaomi redmi 8a phone's IMEI2, How?

Let's see how I edited and flashed a QCN file and restored my IMEI2.

## What do you need?

* Windows (Unfortunately)
* a phone with a Qualcomm chipset
* QCN file for your phone's exact model (Search: qcn file "Name of your phone")
* [ADB drivers](https://adb.clockworkmod.com/)
* [Qualcomm drivers](https://xiaomidriver.com/install-qualcomm-driver)
* [Minimal ADB and fastboot](https://forum.xda-developers.com/t/tool-minimal-adb-and-fastboot-2-9-18.2317790/)
* [QFIL](https://qfiltool.com/)
* [Hex editor](https://mh-nexus.de/en/downloads.php?product=HxD20)

## Let's start :)

**First backup your phone's EFS partition with TWRP (IMPORTANT!)**
on your pc, boot to driver signature enforcement mode.
install ADB drivers,  Qualcomm drivers, and minimal ADB and fastboot.

Now connect your phone, open the dialer app in your stock rom, and type: `*#*#717717#*#*` to enable diag mode and reconnect the phone.
Open QFIL and select the port

from Tools click on QCN Backup restore and restore the QCN file **(if your phone is multi-SIM, enable the multi-SIM option)**.
{{< figure src="b7f0a7ce1387f60ff15dda619d0ef98764ed1ff688becb4a39f7283eecd5b006.png" >}}

Reboot your phone and save the new IMEI2 of your phone somewhere, because we want to restore the original IMEI :)

Convert the new IMEI2 of your phone to hex, and open the QCN file in the hex editor.
{{< figure src="75f050cf83a2543615d0e93189e5178cea68e6d6a70cbd93b8c9f1a9474d874d.png" >}}

Find the hex of your new IMEI2 in the QCN file.
Now convert your original IMEI2 number to hex and replace the new IMEI2 with it and save the edited QCN file.
Reboot to TWRP and restore your EFS partition.

Then reboot the phone, enable diag mode and flash the new QCN file with QFIL.
Reboot again and put your sim card in the second sim slot!
Your IMEI2 is back! :)

**If your phone gets bootloop or breaks in any of these steps, just restore the EFS backup :)**
