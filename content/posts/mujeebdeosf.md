---
title: "Deobfuscating self proclaimed Termux Haxx0r's 'tools'"
date: 2020-05-22T20:15:00+07:00
author: "BeeJumKarray"
---

## First type: The marshal

Algorithm:
--------
```python
import marshal, zlib, base64
exec(marshal.loads(zlib.decompress(base64.decode("Skid stuff here"))))
```

Deobfuscation:
--------------
- Replace `exec()` with a variable claration:
```python
skid = marshal.loads(zlib.decompress(base64.decode("Skid stuff here")))
```

- Add imports that will be used in the latter steps
import py_compile,time,ast

- Write the code object got from marshal to a pyc file:
```python
with open('skid.pyc', 'wb') as fc:
    fc.write('\0\0\0\0')
    py_compile.wr_long(fc, long(time.time()))
    marshal.dump(skid, fc)
    fc.flush()
    fc.seek(0, 0)
    fc.write(py_compile.MAGIC)
```
- Use uncompyle2 to decompile the compiled .pyc file
```sh
python2 scripts/uncompyle2 -o . /home/skid/skid.pyc
```
This should result in a file named output.pyc_dis within the working directory that should contain the decompiled code

## Second type: The onion

Algorithm:
--------
```python
import base64, codecs
magic = "Mujeeb"
love = "is"
god = "big"
destiny = "skid"
joy = '\x72\x6f\x74\x31\x33'
trust = eval('\x6d\x61\x67\x69\x63') + eval('\x63\x6f\x64\x65\x63\x73\x2e\x64\x65\x63\x6f\x64\x65\x28\x6c\x6f\x76\x65\x2c\x20\x6a\x6f\x79\x29') + eval('\x67\x6f\x64') + eval('\x63\x6f\x64\x65\x63\x73\x2e\x64\x65\x63\x6f\x64\x65\x28\x64\x65\x73\x74\x69\x6e\x79\x2c\x20\x6a\x6f\x79\x29')
eval(compile(base64.b64decode(eval('\x74\x72\x75\x73\x74')),'<string>','exec'))
```
Deobfuscation:
--------------
- Replace `eval(compile())` with `print()`:
```python
print(base64.b64decode(eval('\x74\x72\x75\x73\x74')),'<string>','exec')
```
```
- Run the script and pipe the output to a new file
- Chop off the b" in the beginning of the file and the stuff behind the last "
- Then replace the followings:
    + \\ with \
    + \t with
    + \n with newline
- Then run the script and pipe the output to a new file again
- Repeat the following steps from 16 - 19 times because that is big of a skid Mujeeb is
```
This should result in a oneliner containing the original code. doing the 3rd and 4th step again should give you the original code.
