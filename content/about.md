---
title: About
---

## Who are we?
**Kernal** is a small funny, friendly and talented community of *"hackers"* or other weirdos.
And this is our **official blog** coming from our *darkest* and *dankest* headquarters...

Our primary goal is to meme, create and overall have quality time while keeping our user base small and friendly.
We work to make each other stronger through sharing our progress, achievements and failures as well.

## Join our family
If you are a technology-driven, chill person with a passion for FOSS,
who is willing to **actively participate** in the things we do;

enter our dungeon, run the following command:
```sh
curl -s https://gitlab.com/stnby/itguy/raw/master/itguy.sh | sh
```
Join here and show us how it runs: [#join:kernal.eu](https://matrix.to/#/#join:kernal.eu?via=kernal.eu)

Or... chat with us here: `nc chat.kernal.eu 1337` or `torsocks nc cumshitudpemjs4exlopowrjlxj4gw7z3tvelx3wxkbqyihxkulyvpid.onion 1337`
## Other contact information
Email us: **tеаm@kеrnаł.еu**, **staff@kеrnаł.еu**
> Do not copy paste, they are obfuscated because we don't like the taste of spam

You can also find us on:
 + GitLab: [gitlab.com/kernal](https://gitlab.com/kernal) *(IT guy's heaven)*

## Support is welcome
Donate if you would like more of our random content. If you would like us to know who is donating or to leave us a message, visit [donate.kernal.eu](https://donate.kernal.eu).

| Currency         | Address                                                                                           |
| :--------------- | :------------------------------------------------------------------------------------------------ |
| **Monero** (XMR) | `46VGoe3bKWTNuJdwNjjr6oGHLVtV1c9QpXFP9M2P22bbZNU7aGmtuLe6PEDRAeoc3L7pSjfRHMmqpSF5M59eWemEQ2kwYuw` |

```sh
qrencode -tANSI '46VGoe3bKWTNuJdwNjjr6oGHLVtV1c9QpXFP9M2P22bbZNU7aGmtuLe6PEDRAeoc3L7pSjfRHMmqpSF5M59eWemEQ2kwYuw'
```

Or via the government green toilet paper: [Liberapay](https://liberapay.com/kernal/donate), [PayPal](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=CDDAX4F7YT7SG)
