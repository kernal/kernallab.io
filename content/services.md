---
title: Services
---

## Shared shell hosting
Members can host their own services on our VPS. We offer a Rocky Linux 9 server with 8 GB RAM and 4 vCPU.\
We impose resource limits using systemd user slices, see our user management script [here](https://gitlab.com/kernal/shared-shell-management).

If you need to open a port or install a package, feel free to ask an admin.
* SSH: kroket.kernal.eu:22

## Matrix
Registration is not public, you will need to ask an admin to create you an account.
* Homeserver: https://matrix.kernal.eu

## kChat
kChat or Kernal Chat is a functional chat-room server that you can connect using netcat, telnet or any TCP client. We suggest installing rlwrap and connecting with it to prevent overlapping lines:
```sh
rlwrap -S "> " nc chat.kernal.eu 1337
```
IPv6, IPv4 and Tor are available.
* chat.kernal.eu:1337
* cumshitudpemjs4exlopowrjlxj4gw7z3tvelx3wxkbqyihxkulyvpid.onion:1337

## Email
We offer IMAP, POP3, SMTP, rspamd and CalDAV/CardDAV (Radicale). Ask an admin to get an account.
* Webmail: https://mail.kernal.eu
* IMAP: mail.kernal.eu, 993 (TLS) or 143 (STARTTLS)
* SMTP: mail.kernal.eu, 465 (TLS) or 587 (STARTTLS)

## Abuse
We're mostly good netizens. But if you notice something illegal, do tell us.

{{< figure src="/admin.webp" title="Email us: staff@kеrnаł.еu. Protip: We're not Polish." >}}
